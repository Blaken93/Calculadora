/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package calculadora;

import static java.lang.Math.pow;
import java.util.ArrayList;

/**
 *
 * @author Blaken
 */
public class Calculadora {

    public  ArrayList<String> Suma(ArrayList<String> Expresion, ArrayList<String> Datos) {
        int suma, suma_dato = 0;
        ArrayList<String> Resultado = new ArrayList<>();
        boolean sumo = true;
        String aux_Expresion;
        String aux_Datos;
        for (int i = 0; i < Expresion.size(); i++) {
            if (Expresion.get(i).substring(0, 1).equals("-")) {

                suma = -Integer.parseInt(Expresion.get(i).substring(1, 2));
                aux_Expresion = Expresion.get(i).substring(2);
            } else {
                suma = Integer.parseInt(Expresion.get(i).substring(0, 1));
                aux_Expresion = Expresion.get(i).substring(1);
            }sumo=true;
            for (int j = 0; j < Datos.size(); j++) {
                if (Datos.get(j).substring(0, 1).equals("-")) {
                    suma_dato = -Integer.parseInt(Datos.get(j).substring(1, 2));
                    aux_Datos = Datos.get(j).substring(2);
                } else {
                    suma_dato = Integer.parseInt(Datos.get(j).substring(0, 1));
                    aux_Datos = Datos.get(j).substring(1);
                }
                if (aux_Expresion.equals(aux_Datos)) {
                    suma += suma_dato;
                    Resultado.add(String.valueOf(suma) + aux_Datos);
                    Datos.remove(j);
                    sumo = false;
                    break;
                } else {
                    
                    sumo = true;
                }
            }
            if (sumo) {
                Resultado.add(Expresion.get(i));
            }
        }
        Resultado.addAll(Datos);
        Resultado= Simplifica(Resultado,true);
        //System.out.println(Resultado);
        return Resultado;
    }

    public  ArrayList<String> Resta(ArrayList<String> Expresion, ArrayList<String> Datos) {
        int suma, suma_dato = 0;
        ArrayList<String> Resultado = new ArrayList<>();
        boolean sumo = true;
        String aux_Expresion;
        String aux_Datos;
        for (int i = 0; i < Expresion.size(); i++) {
            if (Expresion.get(i).substring(0, 1).equals("-")) {

                suma = -Integer.parseInt(Expresion.get(i).substring(1, 2));
                aux_Expresion = Expresion.get(i).substring(2);
            } else {
                suma = Integer.parseInt(Expresion.get(i).substring(0, 1));
                aux_Expresion = Expresion.get(i).substring(1);
            }sumo=true;
            for (int j = 0; j < Datos.size(); j++) {
                if (Datos.get(j).substring(0, 1).equals("-")) {
                    suma_dato = Integer.parseInt(Datos.get(j).substring(1, 2));
                    aux_Datos = Datos.get(j).substring(2);
                } else {
                    suma_dato = -Integer.parseInt(Datos.get(j).substring(0, 1));
                    aux_Datos = Datos.get(j).substring(1);
                }
                if (aux_Expresion.equals(aux_Datos)) {
                    suma =suma+ suma_dato;
                    Resultado.add(String.valueOf(suma) + aux_Datos);
                    Datos.remove(j);
                    sumo = false;
                    break;
                } else {
                    sumo = true;
                }
            }
            if (sumo) {
                Resultado.add(Expresion.get(i));
            }
        }
        Resultado.addAll(Datos);
        Resultado= Simplifica(Resultado,true);
       // System.out.println(Resultado);
        return Resultado;
        
    }

    public  ArrayList<String> Multiplicacion(ArrayList<String> Expresion, ArrayList<String> Datos) {
        int multiplo, multiplo_dato,x,y = 0;
        ArrayList<String> Resultado = new ArrayList<>();
        String aux_Expresion;
        String aux_Datos;
        for (int i = 0; i < Expresion.size(); i++) {
            if (Expresion.get(i).substring(0, 1).equals("-")) {

                multiplo = -Integer.parseInt(Expresion.get(i).substring(1, 2));
                aux_Expresion = Expresion.get(i).substring(2);
            } else {
                multiplo = Integer.parseInt(Expresion.get(i).substring(0, 1));
                aux_Expresion = Expresion.get(i).substring(1);
            }
            for (int j = 0; j < Datos.size(); j++) {
                if (Datos.get(j).substring(0, 1).equals("-")) {
                    multiplo_dato =- Integer.parseInt(Datos.get(j).substring(1, 2));
                    aux_Datos = Datos.get(j).substring(2);
                } else {
                    multiplo_dato = Integer.parseInt(Datos.get(j).substring(0, 1));
                    aux_Datos = Datos.get(j).substring(1);
                }
                multiplo*=multiplo_dato;
                x= Integer.parseInt(aux_Datos.substring(1, 2)) + Integer.parseInt(aux_Expresion.substring(1, 2));
                y= Integer.parseInt(aux_Datos.substring(3,4)) + Integer.parseInt(aux_Expresion.substring(3,4)); 
                
                Resultado.add(String.valueOf(multiplo) +" "+ String.valueOf(x)+" " + String.valueOf(y));
            }
        }
        //System.out.println(Resultado);
        Resultado= Simplifica(Resultado,true);
       // System.out.println(Resultado);
        return Resultado;
    }

    public int Evaluacion(ArrayList<String> Expresion, int x, int y) {
        int suma=0;
        for (int i = 0; i < Expresion.size(); i++) {
            String Datos = Expresion.get(i);
            suma+= Integer.parseInt(Datos.substring(0,Datos.length()-4))
                    *pow(x,Integer.parseInt(Datos.substring(Datos.length()-3,Datos.length()-2)))
                    *pow(y,Integer.parseInt(Datos.substring(Datos.length()-1,Datos.length())));
            
        }
        return suma;
    }

    private ArrayList<String> Simplifica(ArrayList<String> Expresion,boolean verifica_ceros) {
        ArrayList<String> Resultado = new ArrayList<>();
        int SumaCoeficiente =0;
        String dato1,dato2;
        while(Expresion.size()>0){
            if(Expresion.get(0).substring(0, 1).equals("0")){
                Expresion.remove(0);
            }
            else{
                dato1 =Expresion.get(0);
                SumaCoeficiente= Integer.parseInt(dato1.substring(0, dato1.length()-4));
                for (int i = 1; i < Expresion.size(); i++) {                    
                    dato2=Expresion.get(i);                    
                    if(dato1.substring(dato1.length()-4).equals(dato2.substring(dato2.length()-4))){
                        SumaCoeficiente+=Integer.parseInt(dato2.substring(0, dato2.length()-4));
                        Expresion.remove(i);
                        i--;
                    }
                }
                Resultado.add(String.valueOf(SumaCoeficiente)+dato1.substring(dato1.length()-4));
                Expresion.remove(0);
            }
        }
        if (verifica_ceros) {
            Resultado=Simplifica(Resultado, !verifica_ceros);
        }        
        return Resultado;
    }

}